/**
 * @author Peter Jan Michael Porras
 * var userinfo = require('userinfo').userinfo;
 */

var user = {
	id: null,
	first_name: null,
	last_name: null,
	email: null,
	weight: 0,
	height_feet: 0,
	height_inches: 0,
	birthday: null,
	calories: 0,
	age: null,
	gender: null,
	calories_burned: 0,
	calories_gained: 0,
	weight_gained: 0,
	weight_loss: 0
}; 

exports.info = user;