// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = arguments[0] || {};
var user = require('userInfo');
var toast = require('toast');
var state = 'login';

$.cancel.addEventListener('click', function(e){
	Alloy.Globals.cleanClose( $.signupWindow );
});

$.signupBtn.addEventListener('click',function(e){
	signUp();
});

function signUp(){
	
	var btnTitle = 'Sign Up'; 
	
	var message = '';
	var error = false;
	
	$.signupBtn.applyProperties({ title:"Loading..." });
	
	if ( !isValidEmailAddress($.email.value) ) 
	{	
		message +="Email is invalid."; 
		error=true;			
		
	} else if ( $.email.value < 5 ) {
		
		message +="Email must be greater than 5 characters.\n"; 
		error=true;			
	} 
	
	if ( $.password.value=='' ) 
	{
		message +="Kindly enter your password.";
		error=true;	
	}
	
	if ( $.repeat_password.value != $.password.value ) 
	{
		message +="Your password does not match.";
		error=true;	
	}

	if( error ) 
	{	
		toast.show( message );
		$.signupBtn.applyProperties({ title: 'Sign Up' }) ;
		return;
	}

	var client = Ti.Network.createHTTPClient({ 
	     // function called when the response data is available 
	     onload : function(e) {
	        
	        var response = JSON.parse(this.responseText);
	        console.log(response.message);
	        
			if( response.error ) {
				
				toast.show( response.error );
				$.signupBtn.applyProperties({title:'Sign Up'});
				return;
				
			} else {
					
				user.info = response.data;
				toast.show( response.message );
				// close window and delete all
				Alloy.Globals.cleanClose( $.signupWindow );
			}	 
	     }, 
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	        Ti.API.log(e);
	        console.log(JSON.stringify(e.source));

			toast.show( e.error );
			$.signupBtn.applyProperties({title:'Sign Up'});
	     }, 
	     timeout : 30000  // in milliseconds
	 });
	 
	 // Prepare the connection. 
	 var url = Alloy.Globals.apiBaseUrl+'user';
	 var args = { 
	 	email: $.email.value,
	 	password: $.password.value,
	 	first_name: $.first_name.value,
	 	last_name: $.last_name.value,
	 	password_confirmation: $.repeat_password.value,
	 };
	 
	 // send data
	 client.open("POST", url);
	 client.send( args );
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	return pattern.test(emailAddress);
};

$.signupWindow.addEventListener('open',function(evt){
    evt.source.activity.actionBar.hide();
});

$.signupWindow.open();
