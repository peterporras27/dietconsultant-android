var user = require('userInfo');
var activities = require('activities');
var toast = require('toast');
var state = 'login';

$.BTN_signUp.addEventListener('click', function(e){
	var signupWindow = Alloy.createController('signup',{home:$.index}).getView();
	$.index.open( signupWindow );
});

$.loginBtn.addEventListener('click',function(e){
	if ( $.loginBtn._dissabled == false ) {
		$.loginBtn._dissabled = true;
		userLogin();
	}
});

function userLogin(){
	
	var message = '';
	var error = false;
	
	$.loginBtn.applyProperties({ title:"Loading.." });
	
	if ( !isValidEmailAddress($.email.value) ) 
	{	
		message +="Email is invalid."; 
		error=true;			
		
	} else if ( $.email.value < 5 ) {
		
		message +="Email must be greater than 5 characters.\n"; 
		error=true;			
	} 
	
	if ( $.password.value=='' ) 
	{
		message +="Kindly enter your password."; 
		error=true;	
	}
	
	if( error ) 
	{
		$.loginBtn._dissabled = false;
		toast.show(message);
		$.loginBtn.applyProperties( { title: 'Login' }) ;
		return;
	}
	 
	var client = Ti.Network.createHTTPClient({ 
	     // function called when the response data is available 
	     onload : function(e) {
	        
	        var response = JSON.parse(this.responseText);
	        
			if( response.error ) {
				
				$.loginBtn._dissabled = false;
				toast.show(response.message);
				$.loginBtn.applyProperties({title:'Login'});
				return;
				
			} else {
					
				user.info = response.data;
				$.loginBtn._dissabled = false;
				Ti.App.Properties.setString('token',response.token);
				Alloy.Globals.token = response.token;
				toast.show(response.message);
				if (response.activities) {
					activities.data = response.activities;
				};
				
				Alloy.createController('maintabs').getView().open();
				Alloy.Globals.cleanClose( $.login );
			}
	     }, 
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	        Ti.API.log(e);
	        console.log(JSON.stringify(e.source));
	        $.loginBtn._dissabled = false;
			toast.show( e.message );
			$.loginBtn.applyProperties({title:'Login'});
	     }, 
	     timeout : 30000  // in milliseconds
	 });
	 
	 // Prepare the connection. 
	 var url = Alloy.Globals.apiBaseUrl+'auth/login';
	 var args = { 
	 	email: $.email.value,
	 	password: $.password.value
	 };
	 
	 // send data
	 client.open("POST", url);
	 client.send( args );
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	return pattern.test(emailAddress);
};

$.login.addEventListener('open',function(evt){
    evt.source.activity.actionBar.hide();
});

