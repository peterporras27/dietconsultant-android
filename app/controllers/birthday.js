// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var user = require('userInfo');

var picker = Ti.UI.createPicker({
    type : Ti.UI.PICKER_TYPE_DATE,
    useSpinner : true,
    minDate : new Date(1980, 0, 1),
    maxDate : new Date(),
    value : new Date(1990, 3, 12),
    top : 70,
    width: '90%'
});

var setBtn = Ti.UI.createButton({
    title : 'Save',
    top : 240,
});

setBtn.addEventListener('click', function(e) {
	var pickerdate = picker.value;
    var day = pickerdate.getDate();
    var month = pickerdate.getMonth();
    var year = pickerdate.getFullYear();
    args.data.value = day + "/" + month + "/" + year ;
    $.bdayWin.close();
});

$.bdayWin.add( picker );
$.bdayWin.add( setBtn );
