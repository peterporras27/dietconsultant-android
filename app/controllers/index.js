var user = require('userInfo');
var toast = require('toast');

var state = 'login';
var token = Ti.App.Properties.getString('token');

var activityIndicator = Ti.UI.createActivityIndicator({
  color: '#fff',
  font: {fontFamily:'Helvetica Neue', fontSize:15},
  message: ' Loading...',
  style: Ti.UI.ActivityIndicatorStyle.DARK,
  top:200,
  height:Ti.UI.SIZE,
  width:Ti.UI.FILL
});

if ( Titanium.Network.online == false ) {
	
	Alloy.createController('retry',{home:$.index,url:null}).getView().open();
	console.log(' ');
	console.log('RETRY PAGE');
	console.log(' ');
	
} else {

	$.index.add( activityIndicator );	
	
	$.index.addEventListener('open',function(evt){
	    evt.source.activity.actionBar.hide();
	    activityIndicator.show();
	    loginCheck();
	});
	
	$.index.open();
}

function loginCheck(){
	
	if ( token ) {
		
		var activities = require('activities');
		
		var getUser = Ti.Network.createHTTPClient({
		     // function called when the response data is available
			 onload : function(e) {
			 	
			    var response = JSON.parse(this.responseText);
			    
				if( response.error ) {
					
					Alloy.Globals.cleanClose( $.index );
					Alloy.createController('login').getView().open();
					console.log(' ');
					console.log('LOGIN PAGE');
					console.log(' ');
					
				} else {
					
					user.info = response.data;
					
					if (response.activities) {
						activities.data = response.activities;
					};
					
					Alloy.Globals.token = token;
					Alloy.Globals.cleanClose( $.index );
					Alloy.createController('maintabs').getView().open();
					console.log(' ');
					console.log('MAIN PAGE');
					console.log(' ');
				}
			 },
			 // function called when an error occurs, including a timeout
			 onerror : function(e) {
			     Ti.API.debug(e.error);
			     toast.show(e.message);
			 },
			 timeout : 5000  // in milliseconds
		 });
		 // Prepare the connection.
		 getUser.open("GET", Alloy.Globals.apiBaseUrl+'user?token='+token);
		 // Send the request.
		 getUser.send();
		 
	} else {
		
		Alloy.createController('login',{home:$.index}).getView().open();
		Alloy.Globals.cleanClose( $.index );
		console.log(' ');
		console.log('LOGIN PAGE 2');
		console.log(' ');
	}
}
