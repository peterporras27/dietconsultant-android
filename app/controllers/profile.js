// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var user = require('userInfo');
var toast = require('toast');

loadInfo();

var edit = Ti.UI.createButton({
	title: "Edit Info",
	left: 10, top: 5, right: 10, buttom: 10,
	font: {fontSize:11},
	height: Ti.UI.SIZE,
});

$.profileInfo.add( edit );

var editWindow = Alloy.createController('editProfile',{home:$.profile}).getView();
edit.addEventListener('click', function(e){	
	$.profile.open( editWindow );
});

editWindow.addEventListener('close',function(){
	loadInfo();
});

function loadInfo()
{
	
	$.name.text = user.info.first_name+' '+user.info.last_name;
	$.email.text = user.info.email;
	
	$.details.removeAllChildren();
	
	if ( user.info.birthday ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Birthday: '+user.info.birthday
		}));
	};
	 
	if ( user.info.age ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Age: '+user.info.age
		}));
	};
	
	if ( user.info.gender ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Gender: '+user.info.gender
		}));
	};
	
	if ( user.info.weight ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Weight: '+user.info.weight
		}));
	};
	
	if ( user.info.height_feet ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Height: '+user.info.height_feet+'" '+user.info.height_inches+'\''
		}));
	};
	
	if ( user.info.calories ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Calories: '+user.info.calories
		}));
	};
	
	if ( user.info.calories_burned ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Calories Burned: '+user.info.calories_burned
		}));
	};
	
	if ( user.info.calories_gained ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Calories Gained: '+user.info.calories_gained
		}));
	};
	
	if ( user.info.weight_gained ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Weight Gained: '+user.info.weight_gained
		}));
	};
	
	if ( user.info.weight_loss ) {
		$.details.add(Ti.UI.createLabel({
			text: 'Weight Loss: '+user.info.weight_loss
		}));
	};
}
