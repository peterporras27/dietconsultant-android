// Arguments passed into this controller can be accessed via the `$.args` object directly or:
//var args = $.args;
var args = arguments[0] || {};

var wifi = Ti.UI.createImageView({
	image: '/pics/wifi.png',
	top: 50,
	width: "40%"
});

var grey = Ti.UI.createView({
	width: Ti.UI.FILL, 
	top: 0,
	backgroundColor: "#f9f9f9",
	height: Ti.UI.FILL,
	layout: "vertical"
});

var oops = Ti.UI.createLabel({
	text: "Oops,",
	font:{
		fontSize:15,
		fontWeight: "bold"
	},
	color: "#151515"
});

var note = Ti.UI.createLabel({
	text: "Your internet connection seems to be down.",
	top: 20, font:{fontSize:13},
	color: "#151515"
});

var retry = Ti.UI.createButton({
	title: "Retry",
	top: 10,
	color: "#fff"
});

retry.addEventListener('click',function(e){
	if ( Titanium.Network.online == true ) {	
		var url = (args.url==null) ? null:args.url;
		Alloy.createController('index',{url:url}).getView().open();
		$.retry.close();
	}	
});

grey.add(wifi);
grey.add(oops);
grey.add(note);
grey.add(retry);

$.retry.add( grey );
