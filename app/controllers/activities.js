// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var newWindow = Alloy.createController('activity/new_activity',{home:$.activities}).getView();
var activities = require('activities');

$.newActivity.addEventListener('click',function(){
	$.activities.open( newWindow );
});

newWindow.addEventListener('close',function(e){
	loadActivities();
});

loadActivities();

function loadActivities(){
	
	if ( activities.data.lenght<1 ){ return; }
	
	var lists = [];
	
	_.each( activities.data, function( activity ){
		var row = Ti.UI.createTableViewRow({
			height: 70,
			color:'#2c2c2c',
			backgroundColor: '#2c2c2c',
			_data: activity
		});	
		
		var str = activity.activity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
		    return letter.toUpperCase();
		});
		
		row.title = str;
		
		var title = Ti.UI.createLabel({
			text: str,
			color: '#fff',
			font:{
				fontWeight:'bold',
				fontSize: 17
			},
			top: 10,
			left: 10,
		});
		
		row.add( title );
		
		var stat = '[ '+activity.created_at+' ] - '+ 
			activity.calories_burned+' (cal), '+
			activity.calories_gained+' (cal), '+
			activity.weight_gained+' (lb), '+
			activity.weight_loss+' (lb) ';
		
		var stats = Ti.UI.createLabel({
			text: stat,
			color: '#b6b6b6',
			font:{fontSize: 10},
			top: 40,
			left: 10,
		});
		
		row.add( stats );
		lists.push( row );
		
	});
	
	$.data.setData( lists );
}
