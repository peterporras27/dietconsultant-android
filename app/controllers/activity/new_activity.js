// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var toast = require('toast');
var form = { 
	activity: null,
	notes: '',
	calories_burned: 0,
	calories_gained: 0,
	weight_gained: 0,
	weight_loss: 0
};

$.saveBtn.addEventListener('click',function(){
	if ( $.saveBtn._dissabled == false ) {
		$.saveBtn._dissabled = true;
		saveData();
	}
});

$.activity.addEventListener("change", function(e) {
	form.activity = e.source.columns[0].rows[e.rowIndex].title.toLowerCase();
});

function saveData()
{	
	if ( form.activity == null || form.activity == '--Select' ) {
		toast.show( 'Please select an activity.' );
		$.saveBtn._dissabled = false; 
		return;
	}
	
	var activities = require('activities');
	
	form.notes = $.notes.value;
	form.calories_burned = $.calories_burned.value;
	form.calories_gained = $.calories_gained.value;
	form.weight_gained = $.weight_gained.value;
	form.weight_loss = $.weight_loss.value;
	
	console.log(' ');
	console.log( form );
	console.log(' ');
	
	$.saveBtn.applyProperties({title:'Saving...'});
	var client = Ti.Network.createHTTPClient({ 
	     // function called when the response data is available 
	     onload : function(e) {
	        
	        var response = JSON.parse(this.responseText);
	        
			console.log(' ');
			console.log( response );
			console.log(' ');
	        
			if( response.error ) {
				
				toast.show( response.message );
				$.saveBtn.applyProperties({title:'Save Record'});
				
			} else {
				
				$.saveBtn.applyProperties({title:'Save Record'});
				activities.data.unshift(response.data);
				toast.show( response.message );
				$.activityWindow.close();
			}	
			
			$.saveBtn._dissabled = false; 
	     }, 
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	        Ti.API.log(e);
	        console.log( JSON.stringify(e.source) );
			$.saveBtn._dissabled = false;
			toast.show( e.error );
			$.saveBtn.applyProperties({title:'Save Record'});
	     }, 
	     timeout : 30000  // in milliseconds
	 });
	 
	 // Prepare the connection. 
	 var url = Alloy.Globals.apiBaseUrl+'activity?token='+Alloy.Globals.token;
	 
	 // send data
	 client.open("POST", url);
	 client.send( form );
}

$.calories_burned.addEventListener('focus',function(e){ if (this.value=='0') { this.value = ''; } });
$.calories_burned.addEventListener('blur',function(e){ if (this.value=='') { this.value = '0'; } });

$.calories_gained.addEventListener('focus',function(e){ if (this.value=='0') { this.value = ''; } });
$.calories_gained.addEventListener('blur',function(e){ if (this.value=='') { this.value = '0'; } });

$.weight_gained.addEventListener('focus',function(e){ if (this.value=='0') { this.value = ''; } });
$.weight_gained.addEventListener('blur',function(e){ if (this.value=='') { this.value = '0'; } });

$.weight_loss.addEventListener('focus',function(e){ if (this.value=='0') { this.value = ''; } });
$.weight_loss.addEventListener('blur',function(e){ if (this.value=='') { this.value = '0'; } });
