// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var user = require('userInfo');
var toast = require('toast');
var form = {};

if ( user.info.first_name ) {
	$.first_name.value = user.info.first_name;
};

if ( user.info.last_name ) {
	$.last_name.value = user.info.last_name;
};
 
// if ( user.info.email ) {
	// $.email.value = user.info.email;
// };

if ( user.info.birthday ) {
	$.birthday.value = user.info.birthday;
};

if ( user.info.weight ) {
	$.weight.value = user.info.weight;
};

if ( user.info.gender ) {
	
	if (user.info.gender=='male') { 
		$.gender.setSelectedRow(0, 0, false); 
	}
	
	if ( user.info.gender=='female') {
		$.gender.setSelectedRow(0, 1, false);		
	}
	
	if (user.info.gender=='LGBT') { 
		$.gender.setSelectedRow(0, 2, false); 
	}
};

if ( user.info.calories ) {
	$.calories.value = user.info.calories;
};

var feet = Ti.UI.createPickerColumn();
var inches = Ti.UI.createPickerColumn();

var height_inches = 0;
var height_feet = 0;

for(var i=1,j=20; i<=j; i++){
	feet.addRow(Ti.UI.createPickerRow({ title: i }));
	inches.addRow(Ti.UI.createPickerRow({ title: i }));
	if ( i==user.info.height_inches ) { height_inches = i-1; }
	if ( i==user.info.height_feet ) { height_feet = i-1; }
};

$.height_inches.add([inches]);
$.height_feet.add([feet]);

$.height_inches.setSelectedRow(0, height_inches, false);	
$.height_feet.setSelectedRow(0, height_feet, false);	

var bdayWin = Alloy.createController('birthday',{data:$.birthday, home:args.home}).getView();

$.birthday.addEventListener('focus',function(){args.home.open( bdayWin );});
$.birthday.addEventListener('click',function(){args.home.open( bdayWin );});

$.gender.addEventListener("change", function(e) {
	form.gender = e.source.columns[0].rows[e.rowIndex].title;
});

$.height_feet.addEventListener("change", function(e) {
	form.height_feet = e.source.columns[0].rows[e.rowIndex].title;
});

$.height_inches.addEventListener("change", function(e) {
	form.height_inches = e.source.columns[0].rows[e.rowIndex].title;
});

$.saveBtn.addEventListener('click',function(){
	 
	form.first_name = $.first_name.value;
	form.last_name = $.last_name.value;
	//form.email = $.email.value;
	form.weight = $.weight.value;
	form.birthday = $.birthday.value;
	form.calories = $.calories.value;
	
	$.saveBtn.applyProperties({title:'Loading...'});
	
	var client = Ti.Network.createHTTPClient({ 
	     // function called when the response data is available 
	     onload : function(e) {
	        
	        var response = JSON.parse(this.responseText);
	        console.log(' ');
			console.log( response );
			console.log(' ');
			
			if( response.error ) {
				
				toast.show( response.message );
				
			} else {
					
				user.info = response.data;
				toast.show( response.message );
			}	 
			
			$.saveBtn.applyProperties({title:'Save'});
	     }, 
	     
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	        Ti.API.log(e);
	        console.log( JSON.stringify(e.source) );
			toast.show( e.message );
			$.saveBtn.applyProperties({title:'Save'});
	     }, 
	     timeout : 30000  // in milliseconds
	 });
	 
	// Prepare the connection. 
	var url = Alloy.Globals.apiBaseUrl+'user?token='+Alloy.Globals.token;
	
	form._method = 'PUT';
	   
	if ( $.password.value ) { form.password = $.password.value; };
	if ( $.repeat_password.value ) { form.password_confirmation = $.repeat_password.value; };
	 
 	// send data
	client.open("POST", url);
	client.send( form );
	
	console.log(' ');
	console.log( JSON.stringify( form ) );
	console.log(' ');
});

$.editprofileWindow.addEventListener('open',function(evt){evt.source.activity.actionBar.hide();});

