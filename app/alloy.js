// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.home = '';
Alloy.Globals.token = '';
Alloy.Globals.version = 'v1';
   
Alloy.Globals.serverUrl = 'http://dietconsultant.site/'; 
Alloy.Globals.apiBaseUrl = Alloy.Globals.serverUrl+Alloy.Globals.version+'/';

Alloy.Globals.cleanClose = function( window ) {
	window.removeAllChildren(); 
    window.close();
    window = null;
};

